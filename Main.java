import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        arrayList<String> list = new arrayList<>();
        String str = "Hello world this is my implementation of array list t e s t";
        for(String s : str.split(" ")) {
            list.add(s);
        }

        System.out.println(list);
        System.out.println(list.getSize());
        list.get(5);
        list.remove(5);
        System.out.println(list + " size: " + list.getSize());
        System.out.println(list.contains("this"));
        System.out.println(list.contains("0"));
        list.clear();
        System.out.println(list + " size: " + list.getSize());


    }
}