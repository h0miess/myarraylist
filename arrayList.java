import java.util.Arrays;

public class arrayList<T> {
    private final int INIT_SIZE = 10;
    private int size = INIT_SIZE;
    private Object[] array = new Object[size];
    private int cursor = 0;

    public void add(T obj) {
        if(this.cursor > this.array.length - 1) {
            grow();
        }

        array[cursor++] = obj;
    }

    private void grow() {
        this.array = Arrays.copyOf(array, size * 2);
        this.cursor = size;
        this.size *= 2;
    }

    public T get(int index) {
        if(index > cursor - 1) {
            throw new IllegalArgumentException();
        }
        else {
            return (T) array[index];
        }
    }

    public void remove(int index) {
        if(index >= cursor) throw new IllegalArgumentException();
        Object[] temp = new Object[size];
        int el = 0;
        for(int i = 0; i < cursor; i++) {
            if (i == index) {
                continue;
            }
            temp[el] = array[i];
            el++;
        }
        array = temp;
        cursor--;
    }

    public void remove(T obj) {
        for(int i = 0; i < cursor; i++) {
            if(array[i].equals(obj)) {
                remove(i);
                return;
            }
        }
    }

    public boolean contains(T obj) {
        for(int i = 0; i < cursor; i++) {
            if(array[i].equals(obj)) {
                return true;
            }
        }
        return false;
    }

    public int getSize() {
        return cursor;
    }

    public boolean isEmpty() {
        return cursor == 0;
    }

    public void clear() {
        for(int i = 0; i < cursor; i++) {
            array[i] = null;
        }
        this.cursor = 0;
    }

    public String toString() {
        Object[] temp = new Object[cursor];

        for(int i  = 0; i < cursor; i++) {
            temp[i] = array[i];
        }
        return Arrays.toString(temp);
    }
}
